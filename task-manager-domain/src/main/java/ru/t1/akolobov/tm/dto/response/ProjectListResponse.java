package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.ProjectDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    @NotNull
    private List<ProjectDto> projectList = new ArrayList<>();

    public ProjectListResponse(@NotNull List<ProjectDto> projectList) {
        this.projectList = projectList;
    }

}
