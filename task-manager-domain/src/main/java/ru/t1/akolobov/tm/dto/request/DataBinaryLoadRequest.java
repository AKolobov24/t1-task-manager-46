package ru.t1.akolobov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataBinaryLoadRequest extends AbstractUserRequest {

    public DataBinaryLoadRequest(@Nullable String token) {
        super(token);
    }

}
