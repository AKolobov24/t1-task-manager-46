package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM ProjectDto";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@NotNull String id) {
        return entityManager.find(ProjectDto.class, id);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll() {
        @NotNull String jpql = "SELECT p FROM ProjectDto p";
        return entityManager.createQuery(jpql, ProjectDto.class)
                .getResultList();
    }

    @Override
    public @NotNull List<ProjectDto> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT p FROM ProjectDto p ORDER BY p." + sort.getColumnName();
        return entityManager.createQuery(jpql, ProjectDto.class)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(p) FROM ProjectDto p";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        ProjectDto projectDTO = entityManager.find(ProjectDto.class, id);
        if (projectDTO == null) return;
        entityManager.remove(projectDTO);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull String jpql = "DELETE FROM ProjectDto p WHERE p.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull String userId) {
        @NotNull String jpql = "SELECT p FROM ProjectDto p WHERE p.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull String userId, @NotNull Sort sort) {
        @NotNull String jpql = "SELECT p FROM ProjectDto p WHERE p.userId = :userId " +
                "ORDER BY p." + sort.getColumnName();
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull String jpql = "SELECT p FROM ProjectDto p " +
                "WHERE id = :id AND p.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull String userId) {
        @NotNull String jpql = "SELECT COUNT(p) FROM ProjectDto p WHERE p.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        ProjectDto projectDTO = findOneById(userId, id);
        if (projectDTO == null) return;
        entityManager.remove(projectDTO);
    }

}
