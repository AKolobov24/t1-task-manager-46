package ru.t1.akolobov.tm.api.service.dto;

import ru.t1.akolobov.tm.api.repository.dto.IDtoRepository;
import ru.t1.akolobov.tm.dto.model.AbstractDtoModel;

public interface IDtoService<M extends AbstractDtoModel> extends IDtoRepository<M> {

}
