package ru.t1.akolobov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.enumerated.Status;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {

    @NotNull
    ProjectDto changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    ProjectDto create(@Nullable String userId, @Nullable String name);

    @Nullable
    ProjectDto create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDto create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    ProjectDto updateById(@Nullable String userId,
                          @Nullable String id,
                          @Nullable String name,
                          @Nullable String description);

}
